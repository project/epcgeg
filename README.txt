INTRODUCTION
-----------
This module adds two new field formatters 'EPC' and 'GEG'
and renders this field as a 'EPC' or a 'GEG' diagram.
EPC : Energy Performance Certificate.
GEG : Greenhouse Gas Emissions.


 * For a full description of the module, visit the project page:
   https://drupal.org/project/epcgeg


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/epcgeg


REQUIREMENTS
------------
Drupal 8.x

INSTALLATION
------------
1. Move the entire "epcgeg" directory to
   the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in "Administer" -> "Modules"

3. Choose 'EPC' or 'GEG' as a formatter for your numeric field in
   "Administer" -> "Structure" -> "Content types" -> [Your content type]
    -> "Manage fields"
4. You can there ajust some display settings for your field.

CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------
Current maintainers:
 * Andrzej Wieczorkiewicz (marabak) - https://drupal.org/user/1625476
