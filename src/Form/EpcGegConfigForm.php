<?php

namespace Drupal\epcgeg\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides settings for epcgeg module.
 */
class EpcGegConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'epcgeg_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('epcgeg.settings');
    $form['epc'] = [
      '#type' => 'fieldset',
      '#name' => 'epc_settings',
      '#title' => $this->t('EPC settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $marks = [
      'A' => 50,
      'B' => 90,
      'C' => 150,
      'D' => 230,
      'E' => 330,
      'F' => 450,
    ];
    foreach ($marks as $mark => $value) {
      $form['epc']['epcgeg_epc_' . $mark] = [
        '#type' => 'textfield',
        '#title' => $this->t('@mark max value', ['@mark' => $mark]),
        '#description' => $this->t('The max value for the @mark mark.', ['@mark' => $mark]),
        '#default_value' => $config->get('epcgeg_epc_' . $mark),
      ];
    }

    $form['geg'] = [
      '#type' => 'fieldset',
      '#name' => 'geg_settings',
      '#title' => $this->t('GEG settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $marks = [
      'A' => 5,
      'B' => 10,
      'C' => 20,
      'D' => 35,
      'E' => 55,
      'F' => 80,
    ];
    foreach ($marks as $mark => $value) {
      $form['geg']['epcgeg_geg_' . $mark] = [
        '#type' => 'textfield',
        '#title' => $this->t('@mark max value', ['@mark' => $mark]),
        '#description' => $this->t('The max value for the @mark mark.', ['@mark' => $mark]),
        '#default_value' => $config->get('epcgeg_geg_' . $mark),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('epcgeg.settings');
    foreach (['A', 'B', 'C', 'D', 'E', 'F'] as $mark) {
      $config->set('epcgeg_epc_' . $mark, $form_state->getValue('epcgeg_epc_' . $mark));
      $config->set('epcgeg_geg_' . $mark, $form_state->getValue('epcgeg_geg_' . $mark));
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'epcgeg.settings',
    ];
  }

}
