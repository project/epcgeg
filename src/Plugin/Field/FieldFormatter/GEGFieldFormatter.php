<?php

namespace Drupal\epcgeg\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'gegfield_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "gegfield_formatter",
 *   label = @Translation("GEG field formatter"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class GEGFieldFormatter extends AbstractEpcGegFieldFormatter {

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->graphType = 'epcgeg_geg';
  }

}
