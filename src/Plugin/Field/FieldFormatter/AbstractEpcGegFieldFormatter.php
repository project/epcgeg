<?php

namespace Drupal\epcgeg\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

abstract class AbstractEpcGegFieldFormatter extends FormatterBase {

  /*
   * Theme function to use to render.
   * Possible values : "epcgeg_epc" or "epcgeg_geg".
   */
  protected $graphType;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'width' => 250,
        'height' => 200,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        'width' => [
          '#type' => 'textfield',
          '#title' => $this->t('width'),
          '#element_validate' => ['element_validate_integer_positive'],
          '#default_value' => $this->getSetting('width'),
        ],
        'height' => [
          '#type' => 'textfield',
          '#title' => $this->t('height'),
          '#element_validate' => ['element_validate_integer_positive'],
          '#default_value' => $this->getSetting('height'),
        ],
        // Implement settings form.
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('@width x @height', [
      '@width' => $this->getSetting('width'),
      '@height' => $this->getSetting('height'),
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => $this->graphType,
        '#score' => $this->viewValue($item),
        '#width' => $this->getSetting('width'),
        '#height' => $this->getSetting('height'),
        '#attached' => [
          'library' => [
            'epcgeg/epcgeg',
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
