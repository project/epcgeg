<?php
/**
 * @file
 * Helper class building graphic data to draw the EPC svg diagram.
 */

namespace Drupal\epcgeg;


use Drupal;

/**
 * Class EpcgegEpc.
 */
class EpcgegEpc extends EpcgegGraph {

  /**
   * Epcgeg_epc constructor.
   *
   * @param int $score
   *   The field value.
   * @param int $width
   *   The diagram width.
   * @param int $height
   *   The diagram height.
   * @param int $pad
   *   The space between bars.
   */
  public function __construct($score, $width, $height, $pad) {
    parent::__construct($score, $width, $height, $pad);
    $config = Drupal::config('epcgeg.settings');
    $this->data = [
      [
        'letter' => 'A',
        'bounds' => [NULL, $config->get('epcgeg_epc_A')],
      ],
      [
        'letter' => 'B',
        'bounds' => [
          $config->get('epcgeg_epc_A') + 1,
          $config->get('epcgeg_epc_B'),
        ],
      ],
      [
        'letter' => 'C',
        'bounds' => [
          $config->get('epcgeg_epc_B') + 1,
          $config->get('epcgeg_epc_C'),
        ],
      ],
      [
        'letter' => 'D',
        'bounds' => [
          $config->get('epcgeg_epc_C') + 1,
          $config->get('epcgeg_epc_D'),
        ],
      ],
      [
        'letter' => 'E',
        'bounds' => [
          $config->get('epcgeg_epc_D') + 1,
          $config->get('epcgeg_epc_E'),
        ],
      ],
      [
        'letter' => 'F',
        'bounds' => [
          $config->get('epcgeg_epc_E') + 1,
          $config->get('epcgeg_epc_F'),
        ],
      ],
      [
        'letter' => 'G',
        'bounds' => [$config->get('epcgeg_epc_F'), NULL],
      ],
    ];
    $this->bar_height = ($this->height + $this->pad) / 7 - $this->pad;
    $this->bar_width = $this->width - 3 * $this->bar_height;
  }

  /**
   * Overriding the bar polygon definition.
   *
   * @param int $x
   *   Bar left horizontal position.
   * @param int $y
   *   Bar top vertical position.
   * @param int $x1
   *   Bar right horizontal position.
   * @param int $y1
   *   Bar bottom horizontal position.
   *
   * @return string
   *   Svg polygon points attribute.
   */
  protected function getPolygon($x, $y, $x1, $y1) {
    return implode(' ', [
      $x . ',' . $y,
      $x1 . ',' . $y,
      ($x1 + $this->bar_height / 2) . ',' . (($y + $y1) / 2),
      $x1 . ',' . $y1,
      $x . ',' . $y1,
      $x . ',' . $y,
    ]);
  }

}
