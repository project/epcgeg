<?php
/**
 * @file
 * Helper class building graphic data to draw the EPC ou GEG svg diagram.
 */

namespace Drupal\epcgeg;

/**
 * Class EpcgegGraph.
 */
class EpcgegGraph {

  /**
   * Epcgeg_graph constructor.
   *
   * @param int $score
   *   The field value.
   * @param int $width
   *   The diagram width.
   * @param int $height
   *   The diagram height.
   * @param int $pad
   *   The space between bars.
   */
  public function __construct($score, $width, $height, $pad) {
    $this->score = $score;
    $this->width = $width;
    $this->height = $height;
    $this->pad = $pad;

    $this->data = [];
    $this->bar_height = ($this->height + $this->pad) / 7 - $this->pad;
    $this->bar_width = $this->width - 3 * $this->bar_height;
  }

  /**
   * Builds the data array for the diagram template.
   *
   * @return array
   *   Returns the data array.
   */
  public function getData() {
    $bars = [];
    $count = count($this->data);
    foreach ($this->data as $i => $value) {
      $x = 0;
      $y = $i * ($this->bar_height + $this->pad);
      $x1 = 50 + $i * $this->bar_width / ($count + 1);
      $y1 = $y + $this->bar_height;
      $letter_size = 0.9 * $this->bar_height;
      $legend_size = min(12, 0.6 * $this->bar_height);
      $this->half_bar = ceil($this->bar_height / 2);
      $bar = [
        'polygon' => $this->getPolygon($x, $y, $x1, $y1),
        'letter' => [
          'label' => $value['letter'],
          'x' => $x1 - $this->pad,
          'y' => $y + $this->half_bar,
          'size' => $letter_size,
        ],
        'legend' => [
          'x' => $this->pad,
          'y' => $y + $this->half_bar,
          'size' => $legend_size,
        ],
      ];
      if ($value['bounds'][0] === NULL) {
        $bar['legend']['label'] = '≤ ' . $value['bounds'][1];
      }
      else {
        if ($value['bounds'][1] === NULL) {
          $bar['legend']['label'] = '> ' . $value['bounds'][0];
        }
        else {
          $bar['legend']['label'] = t('@from to @to', [
            '@from' => $value['bounds'][0],
            '@to' => $value['bounds'][1],
          ]);
        }
      }
      $bars[] = $bar;
    }

    return [
      'bars' => $bars,
      'score' => $this->getScoreData(),
    ];
  }

  /**
   * The bar polygon definition.
   *
   * @param int $x
   *   Bar left horizontal position.
   * @param int $y
   *   Bar top vertical position.
   * @param int $x1
   *   Bar right horizontal position.
   * @param int $y1
   *   Bar bottom horizontal position.
   *
   * @return string
   *   Svg polygon points attribute.
   */
  protected function getPolygon($x, $y, $x1, $y1) {
    return implode(' ', [
      $x . ',' . $y,
      $x1 . ',' . $y,
      ($x1 + $this->bar_height / 2) . ',' . (($y + $y1) / 2),
      $x1 . ',' . $y1,
      $x . ',' . $y1,
      $x . ',' . $y,
    ]);
  }

  /**
   * Builds a data array to render the score element of the diagram.
   *
   * @return array
   *   Returns the data array.
   */
  private function getScoreData() {
    foreach ($this->data as $i => $value) {
      if ($value['bounds'][0] === NULL) {
        if ($this->score <= $value['bounds'][1]) {
          $y = $i * ($this->bar_height + $this->pad);
          break;
        }
      }
      else {
        if ($value['bounds'][1] === NULL) {
          if ($value['bounds'][0] <= $this->score) {
            $y = $i * ($this->bar_height + $this->pad);
            break;
          }
        }
        else {
          if ($value['bounds'][0] <= $this->score &&
            $this->score <= $value['bounds'][1]
          ) {
            $y = $i * ($this->bar_height + $this->pad);
            break;
          }
        }
      }
    }
    $size = 0.9 * $this->bar_height;
    return [
      'x' => 0,
      'y' => $y,
      'width' => $this->width,
      'height' => $this->bar_height,
      'label' => [
        'x' => $this->width - $this->pad,
        'y' => $y + $this->half_bar,
        'size' => $size,
      ],
    ];
  }

}
